<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/404','ErrorHandleController@error404');
Route::get('/405','ErrorHandleController@error405');


Route::get('/',[
    'uses' => 'MordernBusinessController@index',
    'as'   => '/'
]);


Route::get('/',[
    'uses' => 'MordernBusinessController@index',
    'as'   => '/'
]);

Route::get('/category-blog/{id}',[
    'uses' => 'MordernBusinessController@categoryBlog',
    'as'   => 'category-blog'
]);

Route::get('/blog-details/{id}',[
    'uses' => 'MordernBusinessController@blogDetails',
    'as'   => 'blog-details'
]);
//sign Up route start from here
Route::get('/sign-up',[
    'uses' => 'SingUpController@singUp',
    'as'   => 'sign-up'
]);
Route::get('/email-check/{email}',[
    'uses' => 'SingUpController@emailCheck',
    'as'   => 'email-check'
]);

Route::post('/new-sign-up',[
    'uses' => 'SingUpController@newSignUp',
    'as'   => 'new-sign-up'
]);

Route::post('/user-logout',[
    'uses' => 'SingUpController@userLogout',
    'as'   => 'user-logout'
]);

Route::get('/user-login',[
    'uses' => 'SingUpController@userLogin',
    'as'   => 'user-login'
]);

Route::post('/user-sign-in',[
    'uses' => 'SingUpController@userSignIn',
    'as'   => 'user-sign-in'
]);
//.........................Comment route start from here..................
Route::post('/new-comment',[
    'uses' => 'CommentController@newComment',
    'as'   => 'new-comment'
]);
Route::get('/add-slider',[
    'uses' => 'SliderController@addSlider',
    'as'   => 'add-slider'
]);
Route::post('/upload-slider',[
    'uses' => 'SliderController@uploadSlider',
    'as'   => 'upload-slider'
]);
Route::get('/manage-slider',[
    'uses' => 'SliderController@manageSlider',
    'as'   => 'manage-slider'
]);
Route::get('/edit-slider/{id}',[
    'uses' => 'SliderController@editSlider',
    'as'   => 'edit-slider'
]);
Route::post('/update-slider',[
    'uses' => 'SliderController@updateSlider',
    'as'   => 'update-slider'
]);
Route::post('/delete-slider',[
    'uses' => 'SliderController@deleteSlider',
    'as'   => 'delete-slider'
]);




//end of the Frontend route here




//here start All admin Panel route
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Category route start here
Route::group(['middleware' => 'super_admin'], function (){
    Route::get('/category/add-category',[
        'uses' => 'CategoryController@addCategory',
        'as' => 'add-category'
    ]);
    Route::post('/category/new-category',[
        'uses' => 'CategoryController@newCategory',
        'as' => 'new-category',
    ]);

    Route::get('/category/manage-category',[
        'uses' => 'CategoryController@manageCategory',
        'as' => 'manage-category'
    ]);

    Route::get('/category/edit-category/{id}',[
        'uses' => 'CategoryController@editCategory',
        'as' => 'edit-category'
    ]);

    Route::post('/category/update-category',[
        'uses' => 'CategoryController@updateCategory',
        'as' => 'update-category'
    ]);
    Route::post('/category/delete-category',[
        'uses' => 'CategoryController@deleteCategory',
        'as' => 'delete-category'
    ]);
});

// category route end here

// blog route start here
Route::group(['middleware' => 'super_admin'], function () {
    Route::get('/blog/add-blog',[
        'uses' => 'BlogController@addBlog',
        'as' => 'add-blog'
    ]);
    Route::post('/blog/new-blog',[
        'uses' => 'BlogController@newBlog',
        'as' => 'new-blog'
    ]);
    Route::get('/blog/manage-blog',[
        'uses' => 'BlogController@manageBlog',
        'as' => 'manage-blog'
    ]);
    Route::get('/blog/edit-blog/{id}',[
        'uses' => 'BlogController@editBlog',
        'as' => 'edit-blog'
    ]);
    Route::post('/blog/update-blog',[
        'uses' => 'BlogController@updateBlog',
        'as' => 'update-blog'
    ]);

    Route::post('/blog/delete-blog',[
        'uses' => 'BlogController@deleteBlog',
        'as' => 'delete-blog'
    ]);
    Route::get('/blog/blog-image-download/{id}',[
        'uses' => 'BlogController@blogImageDownload',
        'as' => 'blog-image-download'
    ]);
});


