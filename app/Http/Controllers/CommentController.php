<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function newComment(Request $request) {
        $comment = new Comment();

        $comment->registration_id       =$request->registration_id;
        $comment->blog_id               =$request->blog_id;
        $comment->comment               =$request->comment;
        $comment->save();

        return redirect('/blog-details/'.$request->blog_id)->with('message','Comment Post Successfully');
    }
}
