<?php

namespace App\Http\Controllers;

use App\Category;
use App\Registration;
use Illuminate\Http\Request;
use Session;

class SingUpController extends Controller
{
    public function singUp()
    {
        return view('front.user.sing-up', [
            'categories' => Category::Where('publication_status', 1)->get()
        ]);
    }

    public function newSignUp(Request $request)
    {
        Registration::registrationInfoSave($request);
        return redirect('/');

    }

    public function userLogout(Request $request)
    {
        Session::forget('registrationID');
        Session::forget('registrationName');
        return redirect('/');
    }

    public function userLogin()
    {
        return view('front.user.user-login', [
            'categories' => Category::Where('publication_status', 1)->get()
        ]);
    }

    public function userSignIn(Request $request)
    {
        $registration = Registration::Where('email', $request->email)->first();
        if ($registration) {
            if (password_verify($request->password, $registration->password)) {

                Session::put('registrationID', $registration->id);
                Session::put('registrationName', $registration->first_name . ' ' . $registration->last_name);

                return redirect('/');
            } else {
                return redirect('/user-login')->with('message', 'Your Password is Wrong!');
            }
        } else {
            return redirect('/user-login')->with('message', 'Your Email Address is Wrong!');
        }

    }
//raw ajax
//    public function emailCheck($email)  {
//       $registration = Registration::where('email',$email)->first();
//        if ($registration){
//            echo 'email address already taken';
//        }
//        else{
//            echo 'email address is available';
//        }
//    }

    public function emailCheck($email)
    {
        $registration = Registration::where('email', $email)->first();
        if ($registration) {
            return json_encode('email address already taken');
        } else {
            return json_decode('email address is available');
        }
    }
}
