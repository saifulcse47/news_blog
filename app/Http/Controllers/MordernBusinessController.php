<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Comment;
use App\Slider;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class MordernBusinessController extends Controller
{
    public function index(){
        return view('front.home.home',[
           'blogs'          => Blog::Where('publication_status',1)->orderBy('id','desc')->simplePaginate(4),
           'categories'     => Category::Where('publication_status',1)->get(),
            'sliders'       => Slider::Where('publication_status',1)->take(1)->get(),
            'slider_twos'   => Slider::Where('publication_status',1)->skip(1)->take(100)->get(),
            'popularBlogs'      => Blog::OrderBy('hit_count','desc')->simplePaginate(3)

        ]);

    }

    public function categoryBlog($id){
        return view('front.category.category-blog',[
            'categories'  => Category::Where('publication_status',1)->get(),
            'blogs'       => Blog::where('category_id',$id)->where('publication_status',1)->get()
        ]);
    }

    public function blogDetails($id){
      $blog = Blog::find($id);
      $blog->hit_count = $blog->hit_count+1;
      $blog->save();

        return view('front.blog.blog-details',[
            'categories'        => Category::Where('publication_status',1)->get(),
            'blog'              => Blog::find($id),
            'comments'          => DB::table('comments')
                                ->join('registrations','comments.registration_id','=', 'registrations.id')
                                ->select('comments.*','registrations.first_name','registrations.last_name')
                                ->where('comments.blog_id',$id)
                                ->orderBy('comments.id','desc')
                                ->get(),

        ]);
    }

}
