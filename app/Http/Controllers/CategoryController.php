<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use Illuminate\Http\Request;
use DB;
class CategoryController extends Controller
{
    public function addCategory(){
        return view('admin.category.add-category');
    }
    public function newCategory(Request $request){

//        DB::table('categories')->insert([
//            'category_name' => $request->category_name,
//            'category_description' => $request->category_description,
//            'publication_status' => $request->publication_status,
//        ]);
//        $category = new Category();
//        $category -> category_name = $request -> category_name;
//        $category -> category_description = $request -> category_description;
//        $category -> publication_status = $request -> publication_status;
//        $category -> save();
        $this->validate($request, [
            'category_name' => 'required',
            'category_description' => 'required|max:100|min:5'
        ]);
        Category::saveCategoryInfo($request);
        return redirect('/category/add-category')->with('message','category info save successfully');

    }
    public function manageCategory(){
        $categories = Category::all();
        return view('admin.category.manage-category',[
            'categories' => Category::all()
        ]);
    }
    public function editCategory($id){
        //$category = Category::find($id);
        return view('admin.category.edit-category',[
            'category' =>  Category::find($id)
        ]);
    }
    public function updateCategory(Request $request){
        Category::updateCategory($request);
        return redirect('/category/manage-category')->with('message','Category info Update Seccessfully');

    }
    public function deleteCategory(Request $request){

       $blog = Blog::Where('category_id',$request->id)->first();
        if($blog){
            return redirect('/category/manage-category')->with('message','Sorry. This Category have some blogs if delete this category blogs are not work');
        }
        else{
            $category = Category::find($request->id);
            $category -> delete();
            return redirect('/category/manage-category')->with('message','Category info Delete Seccessfully');
        }

    }
}
