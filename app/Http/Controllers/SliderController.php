<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function addSlider() {
        return view('admin.slider.add-slider');
    }

    public function uploadSlider(Request $request) {
        $this->checkSliderData($request);
        Slider::uploadSlider($request);
        return redirect('/add-slider')->with('message','Slider Information Save Successfully');
    }
    public function manageSlider() {
        return view('admin.slider.manage-slider',[
          'sliders'  => Slider::all()
        ]);
    }
    public function editSlider($id) {
        return view('admin.slider.edit-slider',[
          'slider' => Slider::find($id)
        ]);
    }
    public function updateSlider(Request $request) {
        Slider::updateSlider($request);
        return redirect('/manage-slider')->with('message','Slider Info Updated Successfully');
    }
    public function deleteSlider(Request $request) {
      $slider = Slider::find($request->id);
      if (file_exists($slider->slider_image)){
          unlink($slider->slider_image);
      }
      $slider->delete();
        return redirect('/manage-slider')->with('message','Slider Info Delete Successfully');
    }
    protected function checkSliderData($request) {
        $this->validate($request,[
            'slider_heading' => 'required',
            'slider_title' => 'required',
            'slider_image' => 'required',
        ]);
    }

}
