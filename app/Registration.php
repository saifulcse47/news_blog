<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use Illuminate\Support\Facades\Mail;

class Registration extends Model
{
    protected $fillable = ['first_name','last_name','email','password','birth_date','phone_number','address','gender'];

    public static function registrationInfoSave($request){
        $registration = new Registration();
        $registration->first_name       =$request->first_name;
        $registration->last_name        =$request->last_name;
        $registration->email            =$request->email;
        $registration->password         =bcrypt($request->password);
        $registration->birth_date       =$request->birth_date;
        $registration->phone_number     =$request->phone_number;
        $registration->address          =$request->address;
        $registration->gender           =$request->gender;
        $registration->save();

        Session::put('registrationID',$registration->id);
        Session::put('registrationName',$registration->first_name.' '. $registration->last_name );

        $data = $registration->toArray();

        Mail::send('front.mail.congratulation',$data ,function ($message) use ($data){
            $message->to($data['email']);
            $message->subject('Congratulation Mail');
        });
        }

}
