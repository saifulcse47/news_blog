<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['slider_heading','slider_title','slider_image','publication_status'];

    public static function uploadSlider($request) {
        $image = $request->file('slider_image');
        $imageName = $image->getClientOriginalName();
        $directory = 'slider-image/';
        $image->move($directory,$imageName);

        $slider = new Slider();
        $slider->slider_heading         = $request->slider_heading;
        $slider->slider_title           = $request->slider_title;
        $slider->slider_image           = $directory.$imageName;
        $slider->publication_status     = $request->publication_status;
        $slider->save();
    }
    public static function updateSlider($request) {
      $slider = Slider::find($request->id);
      $sliderImage = $request->file('slider_image');
      if($sliderImage){
          unlink($slider->slider_image);
          $imageName = $sliderImage->getClientOriginalName();
          $directory = 'slider-image/';
          $sliderImage->move($directory,$imageName);

          $slider->slider_heading           = $request->slider_heading;
          $slider->slider_title             = $request->slider_title;
          $slider->slider_image             = $directory.$imageName;
          $slider->publication_status       = $request->publication_status;
          $slider->save();
      }
      else{
          $slider->slider_heading           = $request->slider_heading;
          $slider->slider_title             = $request->slider_title;
          $slider->publication_status       = $request->publication_status;
          $slider->save();
      }
    }
}
