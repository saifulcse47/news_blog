<?php

namespace App;

use http\Env\Request;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable =['category_id','category_name','blog_title','blog_short_description','blog_long_description','blog_image','publication_status'];

    public static function newBlog($request){
        $image      = $request->file('blog_image');
        $imageName  = $image->getClientOriginalName();
        $directory  = 'blog-image/';
        $image->move($directory,$imageName);

        $blog = new Blog();
        $blog->category_id            =$request->category_id;
        $blog->blog_title             =$request->blog_title;
        $blog->blog_short_description =$request->blog_short_description;
        $blog->blog_long_description  =$request->blog_long_description;
        $blog->blog_image             =$directory.$imageName;
        $blog->publication_status     =$request->publication_status;
        $blog->save();
    }

    public static function updateBlog($request) {
        $blog = Blog::find($request->id);
        $blogImage = $request->file('blog_image');
        if($blogImage) {
            unlink($blog->blog_image);
            $imageName  = $blogImage->getClientOriginalName();
            $directory  = 'blog-image/';
            $blogImage->move($directory,$imageName);

            $blog->category_id            =$request->category_id;
            $blog->blog_title             =$request->blog_title;
            $blog->blog_short_description =$request->blog_short_description;
            $blog->blog_long_description  =$request->blog_long_description;
            $blog->blog_image             =$directory.$imageName;
            $blog->publication_status     =$request->publication_status;
            $blog->save();

        }else{
            $blog->category_id            =$request->category_id;
            $blog->blog_title             =$request->blog_title;
            $blog->blog_short_description =$request->blog_short_description;
            $blog->blog_long_description  =$request->blog_long_description;
            $blog->publication_status     =$request->publication_status;
            $blog->save();

        }
    }
    public static function deleteBlogInfo($request){
        $blog = Blog::find($request->id);
        unlink($blog->blog_image);
        $blog->delete();
    }
}

