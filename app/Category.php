<?php

namespace App;
use http\Env\Request;
use App\Blog;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['category_name','category_description','publication_status'];
    public static function saveCategoryInfo($request){
        Category::create($request->all());
    }
    public static function updateCategory($request){
        $categorey = Category::find($request->id);
        $categorey -> category_name        = $request ->category_name;
        $categorey -> category_description = $request ->category_description;
        $categorey -> publication_status   = $request ->publication_status;
        if ($categorey -> publication_status == '0'){
          $blogs = Blog::where('category_id',$request->id)->get();
          foreach ($blogs as $blog){
              $blog->publication_status = 0;
              $blog->save();
          }
        }
        $categorey->save();

    }
}
