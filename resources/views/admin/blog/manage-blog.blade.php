@extends('admin.master')
@section('title')
    Manage Blog
@endsection
@section('body')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        Manage Blog Tables
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <h1 class="text-center text-success">{{Session::get('message')}}</h1>
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>SL No</th>
                                <th>Category Name</th>
                                <th>Blog Title</th>
                                <th>Blog Short Discription</th>
                                <th>Blog Long Discription</th>
                                <th>Blog Image</th>
                                <th>Publication Status</th>
                                <th>Action</th>
                                <th>Download</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i=1)
                            @foreach($blogs as $blog)
                                <tr class="odd gradeX">
                                    <td>{{ $i++ }}</td>
                                    <td>{{$blog->category_name}}</td>
                                    <td>{{$blog->blog_title}}</td>
                                    <td>{{str_limit($blog->blog_short_description,30)}}</td>
                                    <td class="">{!! str_limit($blog->blog_long_description,40) !!}</td>
                                    <td><img src="{{asset($blog->blog_image)}}" alt="" height="120" width="100"></td>
                                    <td>{{$blog->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                                    <td >
                                        <a href="{{route('edit-blog',['id'=>$blog->id] )}}">Edit</a>
                                        <a href="" class="delete-btn" id="{{$blog->id}}"
                                           onclick="event.preventDefault();
                                               var check = confirm('Are You sure to delete this?');
                                               if(check){
                                               document.getElementById('deleteBlogForm'+'{{$blog->id}}').submit();
                                               }
                                               " >Delete</a>
                                        <form id="deleteBlogForm{{$blog->id}}" action="{{route('delete-blog')}}" method="POST">
                                            @csrf
                                            <input type="hidden" value="{{$blog->id}}" name="id">
                                        </form>
                                    <td>
                                        <a href="{{route('blog-image-download',$blog->id)}}" class="btn btn-outline-warning">
                                            Download
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
@endsection
