@extends('admin.master')
@section('title')
    Add Slider
@endsection
@section('body')
    <div class="row">
        <div class="col-md-12">
            <br/>
            <div class="well">
                <div class="panel-heading text-center">
                    <h3>Add Slider</h3>
                </div>
                <h1 class="text-center text-success">{{Session::get('message')}}</h1>
                <form action="{{route('update-slider')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="control-label col-md-3">Heading</label>
                        <div class="col-md-9">
                            <input type="text" name="slider_heading" value="{{$slider->slider_heading}}" class="form-control" >
                            <input type="hidden" name="id" value="{{$slider->id}}" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Slider Title</label>
                        <div class="col-md-9">
                            <input type="text" name="slider_title" value="{{$slider->slider_title}}" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Slider Image</label>
                        <div class="col-md-9">
                            <input type="file" name="slider_image" accept="image/*"/>
                            <br>
                            <img src="{{asset($slider->slider_image)}}" height="70" width="35" alt="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Publication Status</label>
                        <div class="col-md-9 radio">
                            <label><input type="radio" {{$slider->publication_status == 1 ? 'checked': ''}} name="publication_status" value="1"/>Published</label>
                            <label><input type="radio" {{$slider->publication_status == 0 ? 'checked' : ''}} name="publication_status" value="0"/>UnPublished</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-9 col-md-offset-3">
                            <input type="submit" name="btn " class="btn btn-success btn-block" value="Save Blog Info"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
