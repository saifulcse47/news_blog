@extends('admin.master')
@section('title')
    Manage Slider
@endsection
@section('body')
    <div class="row">
        <div class="col-lg-12">
            <br>
            <div class="panel panel-default">
                <div class="panel-heading text-center">
                    Manage Slider Tables
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <h1 class="text-center text-success">{{Session::get('message')}}</h1>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>SL No</th>
                            <th>Heading</th>
                            <th>Slider Title</th>
                            <th>Slider Image</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i=1)
                        @foreach($sliders as $slider)
                            <tr class="odd gradeX">
                                <td>{{ $i++ }}</td>
                                <td>{{$slider->slider_heading}}</td>
                                <td>{{$slider->slider_title}}</td>
                                <td><img src="{{asset($slider->slider_image)}}" alt="" height="120" width="100"></td>
                                <td>{{$slider->publication_status == 1 ? 'Published' : 'Unpublished'}}</td>
                                <td >
                                    <a href="{{route('edit-slider',['id'=>$slider->id] )}}">Edit</a>
                                    <a href="" class="delete-btn" id="{{$slider->id}}"
                                       onclick="event.preventDefault();
                                               var check = confirm('Are You sure to delete this?');
                                               if(check){
                                               document.getElementById('deleteSliderForm'+'{{$slider->id}}').submit();
                                               }
                                               " >Delete</a>
                                    <form id="deleteSliderForm{{$slider->id}}" action="{{route('delete-slider')}}" method="POST">
                                        @csrf
                                        <input type="hidden" value="{{$slider->id}}" name="id">
                                    </form>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
@endsection

