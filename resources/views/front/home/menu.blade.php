<nav class="navbar full-menu navbar-expand-lg transition" >
    <div class="container p-0 position-relative">
        <a class="navbar-brand p-0" href="{{route('/')}}" style="color: white">Meghna News</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                @foreach($categories as $category)
                <li class="nav-item">
                    <a class="nav-link" href="{{route('category-blog',['id'=>$category->id])}}">{{$category->category_name}}</a>
                </li>
                @endforeach
                @if(Session::get('registrationID'))
                        <li class="nav-item dropdown"><a class="nav-link text-white dropdown-toggle" data-toggle="dropdown" href="">{{Session::get('registrationName')}}</a>
                            <ul class="dropdown-menu">
                                <li><a  class="dropdown-item " href="" onclick="
                                event.preventDefault();
                                document.getElementById('userLogoutForm').submit();
                                 ">Logout</a></li>
                            </ul>
                            <form id="userLogoutForm" action="{{route('user-logout')}}" method="POST">
                                @csrf

                            </form>
                        </li>
                @else
                        <li class="nav-item"><a class="nav-link" href="{{route('user-login')}}">Login</a></li>
                        <li class="nav-item"><a class="nav-link" href="{{route('sign-up')}}">Sign Up</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
