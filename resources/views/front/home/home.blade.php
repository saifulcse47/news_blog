@extends('front.master')
@section('body')
    <header>

        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner" role="listbox">
                <!-- Slide One - Set the background image for this slide in the line below -->
                @foreach($sliders as $slider)
                <div class="carousel-item active" style="background-image: url({{ asset($slider->slider_image) }})">
                    <div class="carousel-caption d-none d-md-block">
                        <h3>{{ $slider->slider_heading }}</h3>
                        <p>{{ $slider->slider_title }}</p>
                    </div>
                </div>
                @endforeach
                <!-- Slide Two - Set the background image for this slide in the line below -->
                @foreach($slider_twos as $slider)
                <div class="carousel-item" style="background-image: url({{ asset($slider->slider_image) }})">
                    <div class="carousel-caption d-none d-md-block">
                        <h3>{{ $slider->slider_heading }}</h3>
                        <p>{{ $slider->slider_title }}</p>
                    </div>
                </div>
                @endforeach
            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>

        </div>
    </header>
    {{--        end of the slider section--}}
    <div class="container container-fluid marquee-section">
        <div class="row container-fluid">
            <marquee behavior="" direction=""><h3>latest news</h3></marquee>
        </div>
    </div>
{{--   latest news section start from here --}}
    <section id="latest-news">
        <div class="container">
            <div class="row">
                <h1 class="my-4">The Latest News</h1>
                <div class="">
                    @foreach($blogs as $blog)
                        <div class="col-6 col-md-3 col-sm-3 col-lg-3 col-xl-3 float-left mb-1">
                            <div class=" h-50">
                                <a href="{{route('blog-details',['id'=>$blog->id])}}" style="color: #0c0c0c">
                                    <img src="{{asset($blog->blog_image)}}" alt="" height="180" width="120" class="card-img-top">
                                    <h4 class="text-center">{{$blog->blog_title}}</h4>
                                    <div class="card-body">
                                        <p class="card-text">{!!str_limit ($blog->blog_long_description,18)!!}</p>
                                    </div>
                                </a>
                                <div class="">
                                    <a href="{{route('blog-details',['id'=>$blog->id])}}" class="btn btn-primary">Learn More</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-lg"></div>
                    <div class="align-self-center"><Center>{{ $blogs->links() }}</Center></div>
                </div>
            </div>
        </div>
    </section>
{{--latest news section end--}}
    <section id="">
            <div class="container">
                <!-- Popular Blogs Section -->
                <h1>Popular Blog</h1>
                <div class="">
                    @foreach($popularBlogs as $popularBlog)
                        <div class="col-6 col-md-3 col-sm-3 col-lg-3 col-xl-3 float-left mb-1">
                            <div class="card h-100">
                                <img src="{{asset($popularBlog->blog_image)}}" alt="" height="200" width="150" class="card-img-top">
                                <h4 class="text-center">{{$popularBlog->blog_title}}</h4>
                                <div class="card-body">
                                    <p class="card-text">{!!str_limit($popularBlog->blog_long_description,18)!!}</p>
                                </div>
                                <div class="card-footer">
                                    <a href="{{route('blog-details',['id'=>$popularBlog->id])}}" class="btn btn-primary">Learn More</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="row container-fluid">
                        <Center>{{ $popularBlogs->links() }}</Center>
                    </div>
                </div>
{{--                end of popular news section--}}
                <!-- /.row -->

                <!-- Features Section -->

            </div>
        <section id="about-section">
         <div class="container mt-4">
             <div class="row">
                 <div class="col-lg-6">
                     <h2>Our Office</h2>
                     <ul>
                         <li>
                             <strong>Meghna,Comilla</strong>
                         </li>
                         <li>Mobile:01915887880</li>
                         <li>Email:saifulcse47@gmail.com</li>
                         <li>What's Apps: 01915887880</li>
                     </ul>
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
                 </div>
                 <div class="col-lg-6">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1291.163539156524!2d90.3836444680982!3d23.748735291904563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b8b06e2d91e9%3A0xc6df244ad130ba2f!2sBashir%20Uddin%20Road%20Jame%20Masjid!5e0!3m2!1sen!2sbd!4v1582197403403!5m2!1sen!2sbd" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                 </div>
             </div>
             <!-- /.row -->

             <hr>

             <!-- Call to Action Section -->
             <div class="row mb-4">
                 <div class="col-md-8">
                     <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias, expedita, saepe, vero rerum deleniti beatae veniam harum neque nemo praesentium cum alias asperiores commodi.</p>
                 </div>
                 <div class="col-md-4">
                     <a class="btn btn-lg btn-secondary btn-block" href="#">Call to Action</a>
                 </div>
             </div>
         </div>
        </section>
    </section>
@endsection
