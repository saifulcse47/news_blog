@extends('front.master')
@section('title')
    Category Blog
@endsection
@section('body')
    <section class="mt-4">
        <div class="container">
            <h1>Well come details</h1>
            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-6 col-md-3 col-sm-3 col-lg-3 col-xl-3 float-left mb-1 ">
                        <div class="card h-100">
                            <img src="{{asset($blog->blog_image)}}" alt="" height="200" width="150" class="card-img-top">
                            <h4 class="text-center">{{$blog->blog_title}}</h4>
                            <div class="card-body">
                                <p class="card-text">{!! str_limit($blog->blog_long_description,50) !!}</p>
                            </div>
                            <div class="card-footer">
                                <a href="{{route('blog-details',['id'=>$blog->id])}}" class="btn btn-primary">Learn More</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
