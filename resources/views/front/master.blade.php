<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Raleway:300,400,500,600,700|Sacramento&display=swap" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href=" {{asset('/')}}assets/front/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{asset('/')}}assets/front/css/modern-business.css" rel="stylesheet">
</head>
<body>
@include('front.home.menu')
<!-- Navigation -->
@yield('body')
<!-- Page Content -->
<!-- /.container -->
<!-- Footer -->
@include('front.home.footer')
<!-- Bootstrap core JavaScript -->
<script src="{{asset('/')}}assets/front/vendor/jquery/jquery.min.js"></script>
<script src="{{asset('/')}}assets/front/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('/')}}assets/front/js/front_main.js"></script>

</body>

</html>
