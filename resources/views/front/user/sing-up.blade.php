@extends('front.master')
@section('title')
    Register Form
@endsection
@section('body')
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <h1 class="mt-4 mb-3"></h1>
        <div class="row">

            <!-- Post Content Column -->
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('new-sign-up')}}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <h2>Registration</h2>
                            <div class="form-group">
                                <label for="firstName" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="first_name" placeholder="First Name" class="form-control" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastName" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="last_name" placeholder="Last Name" class="form-control" autofocus>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-sm-3 control-label">Email* </label>
                                <div class="col-sm-9">
                                    <input type="email" name="email" placeholder="Email" onblur="emailCheck(this.value); " class="form-control">
                                    <span id="res" class="text-center text-success"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">Password*</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password" placeholder="Password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="birthDate" class="col-sm-3 control-label">Date of Birth*</label>
                                <div class="col-sm-9">
                                    <input type="date" name="birth_date" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phoneNumber" class="col-sm-3 control-label">Phone number </label>
                                <div class="col-sm-9">
                                    <input type="number" name="phone_number" class="form-control">
                                </div>
                            </div>
                              <div class="form-group">
                                    <label  class="col-sm-3 control-label">Address</label>
                                    <div class="col-md-9">
                                    <textarea name="address" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3">Gender</label>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="female">Female
                                            </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="radio-inline">
                                                <input type="radio" name="gender" value="male">Male
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- /.form-group -->
                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <span class="help-block">*Required fields</span>
                                </div>
                            </div>
                            <input type="submit" name="btn" id="btnReg" class="btn btn-primary btn-block" value="Register"/>
                        </form> <!-- /form -->
                    </div>
                </div>
            </div>
            <!-- Sidebar Widgets Column -->
            <div class="col-md-4">
                <!-- Search Widget -->
                <div class="card mb-4">
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
                        </div>
                    </div>
                </div>

                <!-- Categories Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Categories</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">Web Design</a>
                                    </li>
                                    <li>
                                        <a href="#">HTML</a>
                                    </li>
                                    <li>
                                        <a href="#">Freebies</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <a href="#">JavaScript</a>
                                    </li>
                                    <li>
                                        <a href="#">CSS</a>
                                    </li>
                                    <li>
                                        <a href="#">Tutorials</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Side Widget -->
                <div class="card my-4">
                    <h5 class="card-header">Side Widget</h5>
                    <div class="card-body">
                        You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
                    </div>
                </div>

            </div>

        </div>
        <!-- /.row -->
    </div>

    <script>
        function emailCheck(email) {
           // var xmlHttp = new XMLHttpRequest();
           // var serverPage ='http://localhost/mordern-business/public/email-check/'+email;
           //  xmlHttp.open('GET',serverPage);
           //  xmlHttp.onreadystatechange = function () {
           //      if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
           //          document.getElementById('res').innerHTML=xmlHttp.responseText;
           //      }
           //      if (xmlHttp.responseText == 'email address already taken') {
           //          document.getElementById('btnReg').disabled = true;
           //      }
           //      else {
           //          document.getElementById('btnReg').disabled = false;
           //
           //      }
           //
           //  }
           //  xmlHttp.send();
            $.ajax({
                url:'http://localhost/mordern-business/public/email-check/'+email,
                method:'GET',
                data:{email:email},
                dataTable:'JSON',
                success:function (res) {
                    document.getElementById('res').innerHTML=res;
                    if(res=='Email address already taken'){
                        document.getElementById(regBtn).disabled=true;
                    }
                    else {
                        document.getElementById(regBtn).disabled=false;

                    }
                }
            })
        }
    </script>
@endsection